function setup(){
    createCanvas(400, 400);
    angleMode(DEGREES);
}

function draw() {
    background(0);
    translate(200, 200);
    rotate(-90);

    noFill();
    strokeWeight(8);

    stroke(255, 100, 150);
    let secondAngle = map(second() , -1, 59, 0, 360);
    arc(0, 0, 300, 300, 0, secondAngle);
    push();
    rotate(secondAngle);
    line(0,0,100,0);
    pop();


    stroke(150, 100, 255);
    let minuteAngle = map(minute() , 0, 60, 0, 360);
    arc(0, 0, 280, 280, 0, minuteAngle);
    push();
    rotate(minuteAngle);
    line(0,0,80,0);
    pop();


    stroke(150, 255, 100);
    let hourAngle = map(hour() % 12, 0, 12, 0, 360);
    arc(0, 0, 260, 260, 0, hourAngle);
    push();
    rotate(hourAngle);
    line(0,0,60,0);
    pop();

}